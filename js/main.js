﻿// arv_forsta has the following structure:
var arv_forsta=[];
var arv_pafolj=[];
var ob_tolktid_ersatt_per_halvtimme=[];
var ob_spilltid_ersatt_per_halvtimme=[];
var helg_tolktid_ersatt_per_halvtimme=[];
var helg_spilltid_ersatt_per_halvtimme=[];

// Maximalt antal timmar per tidsnivå
var max_timmar_per_tidsniva = [1, 1.5, 2, 2.5, 3, 3.5, 4, 4.5, 5, 5.5];

// Associativ mappning för arvodo
// Givet instans (domstol/myndighet)
//   samt kompetensnivå (0 till 3)
//   samt tidsnivå (0 till 9)
//   samt inkludera social avgift (0 eller 1)
//   ger arvodo
// Källa: http://www.domstol.se/Publikationer/Rattshjalp_och_taxor/rattshjalp_och_taxor_2018.pdf
var instans2arvodosniva2tidsniva2inklsoc2arvodo = {
	'domstol': [
		// Arvodosnivå I
		// Första post är för "<= 1 timme", exkl respektive inkl sociala avgifter.
		[[ 352,  463], [ 479,  630], [ 606,  796], [ 733,  963], [ 860, 1130],
		 [ 987, 1297], [1114, 1464], [1241, 1631], [1368, 1798], [1495, 1965]],
		// Arvodosnivå II
		[[ 409,  538], [ 567,  745], [ 725,  953], [ 883, 1160], [1041, 1368],
		 [1199, 1576], [1357, 1783], [1515, 1991], [1673, 2199], [1831, 2406]],
		// Arvodosnivå III
		[[ 480,  631], [ 665,  874], [ 850, 1117], [1035, 1360], [1220, 1603],
		 [1405, 1846], [1590, 2090], [1775, 2333], [1960, 2576], [2145, 2819]],
		// Arvodosnivå IV
		[[ 606,  796], [ 845, 1110], [1084, 1425], [1323, 1739], [1562, 2053],
		 [1801, 2367], [2040, 2681], [2279, 2995], [2518, 3309], [2757, 3623]]
	],
	'myndighet': [
		[[ 293,  385], [ 420,  552], [ 547,  719], [ 674,  886], [ 801, 1053],
		 [ 928, 1220], [1055, 1386], [1182, 1553], [1309, 1720], [1436, 1887]],
		[[ 340,  447], [ 492,  647], [ 644,  846], [ 796, 1046], [ 948, 1246],
		 [1100, 1446], [1252, 1645], [1404, 1845], [1556, 2045], [1708, 2245]],
		[[ 399,  524], [ 577,  758], [ 755,  992], [ 933, 1226], [1111, 1460],
		 [1289, 1694], [1467, 1928], [1645, 2162], [1823, 2396], [2001, 2630]],
		[[ 511,  672], [ 738,  970], [ 965, 1268], [1192, 1567], [1419, 1865],
		 [1646, 2163], [1873, 2461], [2100, 2760], [2327, 3058], [2554, 3356]]
	]
};

// Påslag för påföljande timmar efter första 5.5.
// Källa: http://www.domstol.se/Publikationer/Rattshjalp_och_taxor/rattshjalp_och_taxor_2018.pdf
var instans2arvodosniva2inklsoc2pafoljande_arvodo_per_halvtimme = {
	'domstol': [
		[127, 167],
		[158, 208],
		[185, 243],
		[239, 314]
	],
	'myndighet': [
		[127, 167],
		[152, 200],
		[178, 234],
		[227, 298]
	]
};

//Res/spilltid [ÖT, GT, AT, RT]
// inkl. resp exkl. sociala avgifter
// Per påbörjad timme
var res_ersattning_per_timme = {
	'domstol': [[191, 251], [224, 294], [264, 347], [350, 460]],
	'myndighet': [[191, 251], [224, 294], [264, 347], [350, 460]]
};

//OB Ersättning /0.5h
ob_tolktid_ersatt_per_halvtimme = {
	// Fyra arvodonivåer. Exkl resp inkl sociala avgifter
	'domstol': [[77, 101], [101, 133], [119, 156], [136, 179]],
	'myndighet': [[77, 101], [101, 133], [119, 156], [136, 179]]
};
// Per halvtimme
ob_spilltid_ersatt_per_halvtimme = {
	// Fyra arvodonivåer. Exkl resp inkl sociala avgifter
	'domstol': [[77, 101], [101, 133], [119, 156], [136, 179]],
	'myndighet': [[77, 101], [101, 133], [119, 156], [136, 179]]
};

helg_tolktid_ersatt_per_halvtimme = {
	// Fyra arvodonivåer. Exkl resp inkl sociala avgifter
	'domstol': [[127, 167], [158, 208], [185, 243], [239, 314]],
	'myndighet': [[127, 167], [158, 208], [185, 243], [239, 314]]
};
// Enligt instruktioner används för helgtillägg för spilltid samma värden som
// för OB tillägg för spilltid.
helg_spilltid_ersatt_per_halvtimme = {
	// Fyra arvodonivåer. Exkl resp inkl sociala avgifter
	'domstol': [[77, 101], [101, 133], [119, 156], [136, 179]],
	'myndighet': [[77, 101], [101, 133], [119, 156], [136, 179]]
};

//Kilometerersättning
var kilometerersattning = 3.05;

//Soc Avgifter
var soc_avgifter = 0.3142;

//Moms
var moms = 0.25;

//Förmedlingsavgift
var formedling_avgift = 330;

//Variabler (Default värden) ////////////

var kompetens = 0;				//Input
var tolktyp = 0;				//Input
var obhelg_checked = false;		//Input

//Tolk tid
var tolktid = 0;				//Input
var tolkkostnad_normal_tid = 0;
var tolkkostnad_pafoljande_tid = 0;
var tolkkostnad_sociala_avgifter = 0;
var tolkkostnad_totalt_bas = 0;

//Tolk tid OB
var tolktid_ob_tid = 0;			//Input
var tolktid_ob_kostnad = 0;				
var tolktid_ob_sociala_avgifter = 0;
var tolktid_ob_totalt = 0;

//Tolk tid OB
var tolktid_helg_tid = 0;		//Input
var tolktid_helg_kostnad = 0;				
var tolktid_helg_sociala_avgifter = 0;
var tolktid_helg_totalt = 0;


//Restid (inkl spilltid)
var restid = 0;					//Input
var restid_kostnad = 0;
var restid_sociala_avgifter = 0;
var restid_totalt = 0;

//Res tid OB
var restid_ob_tid = 0;			//Input
var restid_ob_kostnad = 0;
var restid_ob_sociala_avgifter = 0;
var restid_ob_totalt = 0;

//Res tid Helg
var restid_helg_tid = 0;		//Input
var restid_helg_kostnad = 0;
var restid_helg_sociala_avgifter = 0;
var restid_helg_totalt = 0;


//KM Ersättning
var km = 0;						//Input
var km_kostnad = 0;
var km_sociala_avgifter = 0;
var km_totalt = 0;

//Utlägg
var utlagg = 0;					//Input
var utlagg_sociala_avgifter = 0;
var utlagg_totalt = 0;

//Total kostnad
var total_kostnad = 0;
var total_moms = 0;
var total_kostnad_med_moms = 0;

/////////////////////////////////////////

//Extra
var extra_forsta_timme = 5.5;
var extra_pafoljande_tid = tolktid - extra_forsta_timme;
var extra_halvtimmar =  extra_pafoljande_tid / 0.5;

$(document).ready(function() {
	//Set the field values
	setValues();
	updateValues();

	//On keydown round the number upwards and updateValues
	$('#tolktid,#tolktid-ob-tid,#tolktid-helg-tid,#restid-ob-tid,#restid-helg-tid').keyup(function() {		

		//Replace , with .
		$(this).val($(this).val().replace(",", "."));

		//Round to .5
		var tmp=parseFloat($(this).val());
		var tmp_rounded=roundHalf(parseFloat($(this).val()));
		if(tmp!=tmp_rounded && !isNaN(tmp_rounded)){
			$(this).val(tmp_rounded);
		}

		updateValues();
	});


	//On keyup updateValues
	$('#restid,#km,#utlagg').keyup(function() {		
		updateValues();
	});

	//Make sure no non numbers get entered into the restid field
	$("#restid,#km,#utlagg").bind("keypress", function(event) { 
		var charCode = event.which;
		if (charCode <= 13) return true; 

		var keyChar = String.fromCharCode(charCode); 
		return /[0-9]/.test(keyChar); 

	});
	$('#obhelg-checkbox').change(function() {
		obhelg_checked=$(this).is(':checked');
		if(obhelg_checked){
			$('.ob-helg').show(); 	
		}else{
			$('.ob-helg').hide();
		}
		updateValues();
	});

	//Make sure only numbers and . and , are allowed
	$("#tolktid,#tolktid-ob-tid,#tolktid-helg-tid,#restid-ob-tid,#restid-helg-tid").bind("keypress", function(event) { 
		var charCode = event.which;
		if (charCode <= 13) return true; 

		var keyChar = String.fromCharCode(charCode); 
		return /[0-9\.\,]/.test(keyChar); 

	});

	$('input[name=kompetens]').change(function() {
		updateValues();
	});
	$('input[name=tolktyp]').change(function() {
		updateValues();
	});



	$(document).on('mouseenter', '#tolk-ob-explain',  function(){
		$('#tolk-ob-explain-content').css('display', 'inline');
	}).on('mouseleave', '#tolk-ob-explain', function() {
		$('#tolk-ob-explain-content').hide();
	});

	$(document).on('mouseenter', '#tolk-helg-explain',  function(){
		$('#tolk-helg-explain-content').css('display', 'inline');
	}).on('mouseleave', '#tolk-helg-explain', function() {
		$('#tolk-helg-explain-content').hide();
	});
	$(document).on('mouseenter', '#restid-ob-explain',  function(){
		$('#restid-ob-explain-content').css('display', 'inline');
	}).on('mouseleave', '#restid-ob-explain', function() {
		$('#restid-ob-explain-content').hide();
	});
	$(document).on('mouseenter', '#restid-helg-explain',  function(){
		$('#restid-helg-explain-content').css('display', 'inline');
	}).on('mouseleave', '#restid-helg-explain', function() {
		$('#restid-helg-explain-content').hide();
	});

	$(document).on('mouseenter', '#restid-explain',  function(){
		$('#restid-explain-content').css('display', 'inline');
	}).on('mouseleave', '#restid-explain', function() {
		$('#restid-explain-content').hide();
	});
	$(document).on('mouseenter', '#utlagg-explain',  function(){
		$('#utlagg-explain-content').css('display', 'inline');
	}).on('mouseleave', '#utlagg-explain', function() {
		$('#utlagg-explain-content').hide();
	});

})

function setValues(){
	//Tolktid
	$("#tolktid-normal-tid").html(tolkkostnad_normal_tid.toFixed(2));
	$("#tolktid-pafoljande-halvtimmar").html(tolkkostnad_pafoljande_tid.toFixed(2));
	$("#tolktid-sociala-avgifter").html(tolkkostnad_sociala_avgifter.toFixed(2));
	$("#tolktid-totalt").html(tolkkostnad_totalt_bas.toFixed(2));

	//Tolktid OB
	$("#tolktid-ob-kostnad").html(tolktid_ob_kostnad.toFixed(2));
	$("#tolktid-ob-sociala-avgifter").html(tolktid_ob_sociala_avgifter.toFixed(2));
	$("#tolktid-ob-totalt").html(tolktid_ob_totalt.toFixed(2));

	//Tolktid HELG
	$("#tolktid-helg-kostnad").html(tolktid_helg_kostnad.toFixed(2));
	$("#tolktid-helg-sociala-avgifter").html(tolktid_helg_sociala_avgifter.toFixed(2));
	$("#tolktid-helg-totalt").html(tolktid_helg_totalt.toFixed(2));

	//Restid
	$("#restid-kostnad").html(restid_kostnad.toFixed(2));
	$("#restid-sociala-avgifter").html(restid_sociala_avgifter.toFixed(2));
	$("#restid-totalt").html(restid_totalt.toFixed(2));

	//Restid OB
	$("#restid-ob-kostnad").html(restid_ob_kostnad.toFixed(2));
	$("#restid-ob-sociala-avgifter").html(restid_ob_sociala_avgifter.toFixed(2));
	$("#restid-ob-totalt").html(restid_ob_totalt.toFixed(2));

	//Restid HELG
	$("#restid-helg-kostnad").html(restid_helg_kostnad.toFixed(2));
	$("#restid-helg-sociala-avgifter").html(restid_helg_sociala_avgifter.toFixed(2));
	$("#restid-helg-totalt").html(restid_helg_totalt.toFixed(2));

	

	
	//KM Ersättning
	$("#km-kostnad").html(km_kostnad.toFixed(2));
	$("#km-sociala-avgifter").html(km_sociala_avgifter.toFixed(2));
	$("#km-totalt").html(km_totalt.toFixed(2));

	//Utlägg Ersättning
	$("#utlagg-sociala-avgifter").html(utlagg_sociala_avgifter.toFixed(2));
	$("#utlagg-totalt").html(utlagg_totalt.toFixed(2));

	//Förmedlingsavgift
	$("#formedlingsavgift").html(formedling_avgift);

	//Totalt resultat
	$("#total-kostnad").html(total_kostnad.toFixed(2));
	$("#total-moms").html(total_moms.toFixed(2));
	$("#total-kostnad-med-moms").html(total_kostnad_med_moms.toFixed(2));

}

function updateValues(){

	//console.log("Updating values...");

	kompetens = $('input[name=kompetens]:checked').val(); // 0,1,2,3 refererar till index i konstater
	tolktyp = $('input[name=tolktyp]:checked').val(); //0,1 refererar till index i konstater

	tolktid = 0.5*Math.ceil(2*Number($("#tolktid").val()));
	restid = Math.ceil(Number($("#restid").val()));
	km = Math.ceil(Number($("#km").val()));
	utlagg = Math.ceil(Number($("#utlagg").val()));

	tolktid_ob_tid = Number($("#tolktid-ob-tid").val());
	tolktid_helg_tid = Number($("#tolktid-helg-tid").val());
	restid_ob_tid = Number($("#restid-ob-tid").val());
	restid_helg_tid = Number($("#restid-helg-tid").val());

	//console.log($("#tolktid").val());
	//console.log("Kompetens " + kompetens);
	//console.log("Tolktyp " + tolktyp);
	//console.log("Tolktid " + tolktid);
	//console.log("Tolktid_ob_tid " + tolktid_ob_tid);
	//console.log("Tolktid_helg_tid " + tolktid_helg_tid);
	//console.log("Restid_ob_tid " + restid_ob_tid);
	//console.log("Restid_helg_tid " + restid_helg_tid);
	//console.log("KM "+ km);
	//console.log("Utlägg "+ utlagg);

	if(tolktyp !=undefined && kompetens!=undefined){	
		updateTolkTid();
		updateResTid();
		updateKM();
		updateUtlagg();
		updateTolkOB();
		updateTolkHelg();
		updateResOB();
		updateResHelg();

		updateTotalKostnad();

	}else{
		alert("Du måste välja kompetens och tolktyp");
	}

	setValues();


}
function updateTotalKostnad(){
	total_kostnad = utlagg_totalt + km_totalt + tolkkostnad_totalt_bas +
		restid_totalt + formedling_avgift;
	if(obhelg_checked) {
		total_kostnad = total_kostnad + tolktid_ob_totalt +
			tolktid_helg_totalt + restid_ob_totalt + restid_helg_totalt;
	}
	total_moms = total_kostnad * moms;
	total_kostnad_med_moms = total_kostnad + total_moms;
}

function updateUtlagg(){
	if(utlagg>0){
		utlagg_sociala_avgifter = utlagg * soc_avgifter;
		utlagg_totalt = utlagg + utlagg_sociala_avgifter;
	}else{
		utlagg_sociala_avgifter = 0;
		utlagg_totalt = 0;
	}

}

function updateTolkOB(){
	if(tolktid_ob_tid>0){
		//Pris är i halvtimmar omvandlar till heltimmar
		tolktid_ob_kostnad = (tolktid_ob_tid*2) *
			ob_tolktid_ersatt_per_halvtimme[tolktyp][kompetens][0];
		var tolktid_ob_kostnad_inklsoc = (tolktid_ob_tid*2) *
			ob_tolktid_ersatt_per_halvtimme[tolktyp][kompetens][1];
		tolktid_ob_sociala_avgifter = tolktid_ob_kostnad_inklsoc -
			tolktid_ob_kostnad;
		tolktid_ob_totalt = tolktid_ob_kostnad_inklsoc;
	}else{
		tolktid_ob_kostnad = 0;
		tolktid_ob_sociala_avgifter = 0;
		tolktid_ob_totalt = 0;
	}

}

function updateTolkHelg(){
	if(tolktid_helg_tid>0){
		//Pris är i halvtimmar omvandlar till heltimmar
		tolktid_helg_kostnad = (tolktid_helg_tid*2) *
			helg_tolktid_ersatt_per_halvtimme[tolktyp][kompetens][0];
		var tolktid_helg_kostnad_inklsoc = (tolktid_helg_tid*2) *
			helg_tolktid_ersatt_per_halvtimme[tolktyp][kompetens][1];
		tolktid_helg_sociala_avgifter = tolktid_helg_kostnad_inklsoc -
			tolktid_helg_kostnad;
		tolktid_helg_totalt = tolktid_helg_kostnad_inklsoc;
	}else{
		tolktid_helg_kostnad = 0;
		tolktid_helg_sociala_avgifter = 0;
		tolktid_helg_totalt = 0;
	}

}

function updateResOB(){
	if(restid_ob_tid>0){
		//Pris är i halvtimmar omvandlar till heltimmar
		restid_ob_kostnad = (restid_ob_tid*2) *
			ob_spilltid_ersatt_per_halvtimme[tolktyp][kompetens][0];
		var restid_ob_kostnad_inklsoc = (restid_ob_tid*2) *
			ob_spilltid_ersatt_per_halvtimme[tolktyp][kompetens][1];
		restid_ob_sociala_avgifter = restid_ob_kostnad_inklsoc -
			restid_ob_kostnad;
		restid_ob_totalt = restid_ob_kostnad_inklsoc;
	}else{
		restid_ob_kostnad = 0;
		restid_ob_sociala_avgifter = 0;
		restid_ob_totalt = 0;
	}

}

function updateResHelg(){
	if(restid_helg_tid>0){
		//Pris är i halvtimmar omvandlar till heltimmar
		restid_helg_kostnad = (restid_helg_tid*2)
			* helg_spilltid_ersatt_per_halvtimme[tolktyp][kompetens][0];
		var restid_helg_kostnad_inklsoc = (restid_helg_tid*2)
			* helg_spilltid_ersatt_per_halvtimme[tolktyp][kompetens][1];
		restid_helg_sociala_avgifter = restid_helg_kostnad_inklsoc -
			restid_helg_kostnad;
		restid_helg_totalt = restid_helg_kostnad_inklsoc;
	}else{
		restid_helg_kostnad = 0;
		restid_helg_sociala_avgifter = 0;
		restid_helg_totalt = 0;
	}	
}

function updateKM(){
	if(km>0){
		km_kostnad = km * kilometerersattning;
		km_sociala_avgifter = km_kostnad * soc_avgifter;
		km_totalt = km_kostnad + km_sociala_avgifter;
	}else{
		km_kostnad = 0;
		km_sociala_avgifter = 0;
		km_totalt = 0;
	}

}

function updateTolkTid() {
	extra_pafoljande_tid = Math.max(0, tolktid - extra_forsta_timme);
	extra_halvtimmar = extra_pafoljande_tid / 0.5;

	//console.log("påföljande tid " + extra_pafoljande_tid);
	//console.log("halvtimmar " + extra_halvtimmar);

	// Find max level for fixed fees.
	var tidsniva = 0;
	for (; (tidsniva < max_timmar_per_tidsniva.length-1)
		&& max_timmar_per_tidsniva[tidsniva] + Number.EPSILON < tolktid;
		++tidsniva)
		;

	var m = instans2arvodosniva2tidsniva2inklsoc2arvodo;
	tolkkostnad_normal_tid = m[tolktyp][kompetens][tidsniva][0];
	tolkkostnad_normal_tid_inklsoc = m[tolktyp][kompetens][tidsniva][1];
	if (tolktid <= Number.EPSILON) {
		tolkkostnad_normal_tid_inklsoc = 0;
		tolkkostnad_normal_tid = 0;
	}

	var m = instans2arvodosniva2inklsoc2pafoljande_arvodo_per_halvtimme;
	tolkkostnad_pafoljande_tid =
		m[tolktyp][kompetens][0] * extra_halvtimmar;
	var tolkkostnad_pafoljande_tid_inklsoc =
		m[tolktyp][kompetens][1] * extra_halvtimmar;

	tolkkostnad_sociala_avgifter = tolkkostnad_normal_tid_inklsoc -
		tolkkostnad_normal_tid +
		tolkkostnad_pafoljande_tid_inklsoc -
		tolkkostnad_pafoljande_tid;

	tolkkostnad_totalt_bas = tolkkostnad_normal_tid_inklsoc +
		tolkkostnad_pafoljande_tid_inklsoc;
}

function updateResTid(){
	if(restid > 0){
		restid_kostnad = restid *
			res_ersattning_per_timme[tolktyp][kompetens][0];
		var restid_kostnad_inklsoc = restid *
			res_ersattning_per_timme[tolktyp][kompetens][1];
		restid_sociala_avgifter = restid_kostnad_inklsoc -
			restid_kostnad;
		restid_totalt = restid_kostnad_inklsoc;
	}else{
		restid_kostnad = 0;
		restid_sociala_avgifter = 0;
		restid_totalt = 0;
	}
}

function roundHalf(num) {
	num = Math.ceil(num*2)/2;
	return num;
}

